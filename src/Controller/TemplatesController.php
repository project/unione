<?php

namespace Drupal\unione\Controller;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unione\UnioneClient;

/**
 * Show Unione templates info.
 */
class TemplatesController extends ControllerBase {

  /**
   * The Unione client.
   *
   * @var \Unione\UnioneClient
   */
  protected UnioneClient $client;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Unione template constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   The Unione settings config instance.
   */
  public function __construct(ImmutableConfig $settings) {
    $this->config = $settings;
    $this->client = new UnioneClient($this->config->get('api_key'), $this->config->get('endpoint'));
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')->get('unione.settings')
    );
  }

  /**
   * Shows all available Unione templates.
   *
   * @return array
   *   The render array with Unione template info.
   */
  public function viewAll() {

    if (empty($this->config->get('api_key'))) {
      \Drupal::messenger()->addWarning($this->t('API key is empty! Please check Unione settings page.'));
      return [];
    }

    try {
      $templates_list = $this->client->templates()->list();
    }
    catch (\Exception $error) {
      \Drupal::messenger()->addWarning($error->getMessage());
      return [];
    }

    // Creates table for the show templates info.
    $headers = [
      'name' => $this->t('Name'),
      'id' => $this->t('ID'),
      'created' => $this->t('Created'),
      'engine' => $this->t('Engine'),
      'operations' => $this->t('Operations'),
    ];

    $rows = [];
    if ($templates_list['status'] == 'success') {
      $link_options = [
        'attributes' => [
          'class' => [
            'use-ajax',
          ],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => 700,
          ]),
        ],
      ];
      foreach ($templates_list['templates'] as $item) {
        $url = Url::fromRoute('unione.template_view', ['template_id' => $item['id']]);
        $url->setOptions($link_options);

        $rows[] = [
          $item['name'],
          $item['id'],
          $item['created'],
          $item['template_engine'],
          [
            'data' => [
              '#type' => 'operations',
              '#links' => [
                'view' => [
                  'title' => $this->t('View'),
                  'url' => $url,
                ],
              ],
            ],
          ],
        ];
      }
    }

    return [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No templates found. You can create a new template in Unione dashboard (Tools > Templates).'),
    ];
  }

  /**
   * Shows the Unione template info by template ID.
   *
   * @param string $template_id
   *   The template ID.
   *
   * @return array
   *   The render array with Unione template fields info.
   */
  public function view(string $template_id): array {
    try {
      $template_info = $this->client->templates()->get($template_id);
    }
    catch (\Exception $error) {
      \Drupal::messenger()->addWarning($error->getMessage());
      return [];
    }

    $template_fields = $this->getTemplateSubstitutions($template_info['template']);

    // Creates template body render array.
    $body = [];
    if (isset($template_info['template']['body']) && !empty($template_info['template']['body']['html'])) {
      $body = [
        '#type' => 'fieldset',
        '#title' => $this->t('Template Body'),
        'html' => [
          '#markup' => $template_info['template']['body']['html'],
        ],
      ];
    }

    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['unione-template-item'],
      ],
      '#attached' => [
        'library' => [
          'unione/unione.templates',
        ],
      ],
      'content' => [
        [
          '#type' => 'item',
          '#title' => $this->t('ID'),
          '#markup' => $template_info['template']['id'],
        ],
        [
          '#type' => 'item',
          '#title' => $this->t('Subject'),
          '#markup' => $template_info['template']['subject'],
        ],
        [
          '#type' => 'item',
          '#title' => $this->t('From'),
          '#markup' => "{$template_info['template']['from_name']} &#60;{$template_info['template']['from_email']}&#62;",
        ],
        $template_fields,
        $body,
      ],
    ];
  }

  /**
   * Find expected template substitutions in email components.
   *
   * @param array $template
   *   The email Body HTML.
   *
   * @return array|null
   *   List of data fields.
   */
  public function getTemplateSubstitutions(array $template) {
    $fields_arr = [];
    $pattern = '/{\{[^{}]*\}\}/';

    if (!empty($template['subject']) && preg_match_all($pattern,
        $template['subject'], $matches)) {
      $fields_arr = array_merge($fields_arr, $matches[0]);
    }
    if (!empty($template['from_name']) && preg_match_all($pattern,
        $template['from_name'], $matches)) {
      $fields_arr = array_merge($fields_arr, $matches[0]);
    }
    if (!empty($template['body']['html']) && preg_match_all($pattern,
        $template['body']['html'], $matches)) {
      $fields_arr = array_merge($fields_arr, $matches[0]);
    }
    if (!empty($template['body']['amp']) && preg_match_all($pattern,
        $template['body']['amp'], $matches)) {
      $fields_arr = array_merge($fields_arr, $matches[0]);
    }
    if (!empty($template['body']['plaintext']) && preg_match_all($pattern,
        $template['plaintext']['plaintext'], $matches)) {
      $fields_arr = array_merge($fields_arr, $matches[0]);
    }

    if (empty($fields_arr)) {
      return NULL;
    }

    $fields = [];
    foreach (array_unique($fields_arr) as $item) {
      $fields[] = trim($item, '{}');
    }

    return [
      'container' => [
        '#type' => 'item',
        '#title' => $this->t('Expected substitutions'),
        [
          '#theme' => 'item_list',
          '#items' => $fields,
        ],
      ],
    ];
  }

}
