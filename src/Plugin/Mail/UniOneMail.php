<?php

namespace Drupal\unione\Plugin\Mail;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;
use Html2Text\Html2Text;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unione\UnioneClient;

/**
 * Default UniOne mail system plugin.
 *
 * @Mail(
 *   id = "unione_mail",
 *   label = @Translation("UniOne mailer"),
 *   description = @Translation("Sends the message using UniOne.")
 * )
 */
class UniOneMail implements MailInterface, ContainerFactoryPluginInterface {

  /**
   * A regex that matches a structure like 'Name <email@address.com>'.
   *
   * It matches anything between the first < and last > as email address.
   * This allows to use a single string to construct an Address,
   * which can be convenient to use in
   * config, and allows to have more readable config.
   * This does not try to cover all edge cases for address.
   */
  private const FROM_STRING_PATTERN = '~(?<displayName>[^<]*)<(?<addrSpec>.*)>[^>]*~';

  /**
   * UniOne client.
   *
   * @var \Unione\UnioneClient
   */
  protected UnioneClient $client;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The file storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $fileStorage;

  /**
   * The MIME type guesser.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected MimeTypeGuesserInterface $mimeTypeGuesser;

  /**
   * The debug mode.
   *
   * @var bool
   */
  protected bool $isDebug;

  /**
   * UniOneMail constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   A UniOne settings config instance.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityStorageInterface $file_storage
   *   The file storage service.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mime_type_guesser
   *   The MIME type interface.
   */
  public function __construct(ImmutableConfig $settings, LoggerInterface $logger, EntityStorageInterface $file_storage, MimeTypeGuesserInterface $mime_type_guesser) {
    $this->config = $settings;
    $this->logger = $logger;
    $this->fileStorage = $file_storage;
    $this->client = new UnioneClient($this->config->get('api_key'), $this->config->get('endpoint'));
    $this->isDebug = $this->config->get('debug_mode') ?? FALSE;
    $this->mimeTypeGuesser = $mime_type_guesser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory')->get('unione.settings'),
      $container->get('logger.factory')->get('unione'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('file.mime_type.guesser.extension')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {
    [$params, $headers] = $this->buildMessage($message);

    try {
      $response = $this->client->emails()->send($params, $headers);
    }
    catch (\Exception $e) {
      $this->logger->error('Unable to send message from %from to %to: %code %message',
        [
          '%from' => $message['from'],
          '%to' => $message['to'],
          '%code' => 'Error code: ' . $e->getCode(),
          '%message' => $e->getMessage(),
        ]);
    }

    if (!empty($response['status']) && $response['status'] === 'success') {
      // Debug mode: log all messages.
      if ($this->isDebug) {
        $this->logger->notice('Successfully queued message from %from to %to.', [
          '%from' => $message['from'],
          '%to' => implode(', ', $response['emails']),
        ]);
      }
    }

    if (!empty($response['failed_emails'])) {
      $failed_emails =& $decode_response['failed_emails'];
      array_walk($failed_emails, static function (&$value, $key) {
        $value = "{$key}: {$value}";
      });

      $this->logger->error('Unable to send message from %from to: %failed_emails. %code %message',
        [
          '%from' => $message['from'],
          '%code' => $decode_response['code'] ? 'Error code: ' . $decode_response['code'] . '.' : '',
          '%message' => $decode_response['message'] ? 'Error message: ' . $decode_response['message'] . '.' : '',
          '%failed_emails' => $failed_emails
            ? implode(', ', $failed_emails) . '.'
            : '',
        ]);
    }

    return isset($response) && $response['status'] === 'success';
  }

  /**
   * Builds the e-mail message in preparation to be sent to UniOne.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *   $message['params'] may contain additional parameters.
   *
   * @return array
   *   An email array formatted for UniOne delivery.
   *
   * @see https://docs.unione.io/en/web-api-ref?php#email-send
   */
  protected function buildMessage(array $message): array {
    // Add default values to make sure those array keys exist.
    $message += [
      'body' => [],
      'params' => [],
    ];

    $recipients = [];
    // Support custom recipients array with substitutions and metadata.
    if (!empty($message['params']['recipients'])) {
      $recipients = $message['params']['recipients'];
    }

    [$email, $name] = $this->checkEmail($message['headers']['From']);

    if ($message['body'] instanceof MarkupInterface) {
      $message['body'] = $message['body']->__toString();
    }
    elseif (is_array($message['body'])) {
      $message['body'] = implode("\n", $message['body']);
    }

    // @todo Check for empty $message['body']
    // Build the UniOne message array.
    $unione_message = [
      'from_email' => $email,
      'from_name' => $name,
      'recipients' => array_values($recipients),
      'subject' => $message['subject'],
      'body' => [
        'html' => $message['body'],
      ],
      'inline_attachments' => [],
    ];

    // Set inline attachments.
    if (isset($message['params']['inline_attachments'])) {
      $unione_message['inline_attachments'] = array_merge($unione_message['inline_attachments'],
        $message['params']['inline_attachments']);
    }

    // Remove HTML version if the message does not support HTML.
    if (isset($message['params']['html']) && !$message['params']['html']) {
      unset($unione_message['body']['html']);
    }

    // Set text version of the message.
    if (isset($message['plain'])) {
      $unione_message['body']['plaintext'] = $message['plain'];
    }
    else {
      $converter = new Html2Text($message['body'], ['width' => 0]);
      $unione_message['body']['plaintext'] = $converter->getText();
    }

    // Add Reply-To as header according to UniOne API.
    if (!empty($message['reply-to'])) {
      [$email] = $this->checkEmail($message['reply-to']);
      $unione_message['reply_to'] = $email;
    }

    if ($this->isDebug) {
      // Left a message in watchdog with information about Cc and Bcc,
      // because they are not supported by UniOne.
      if (!empty($message['headers']['Cc'])) {
        $this->logger->notice('Cc header is not supported by UniOne.');
      }
      if (!empty($message['headers']['Bcc'])) {
        $this->logger->notice('Bcc header is not supported by UniOne.');
      }
    }

    $unione_message['track_links'] = $this->config->get('track_click');
    $unione_message['track_read'] = $this->config->get('track_read');

    // Removing elements from the header array that are static in the request.
    if (isset($message['headers']['Content-Type'])) {
      unset($message['headers']['Content-Type']);
    }
    if (isset($message['headers']['Accept'])) {
      unset($message['headers']['Accept']);
    }

    // Support Unione templates.
    if (!empty($message['params']['template_id'])) {
      $unione_message['template_id'] = $message['params']['template_id'];

      // If template is set, we assume the default email body should be removed.
      // List of template fields can be provided in 'template_fields' parameter.
      if (!isset($message['params']['template_fields'])) {
        $message['params']['template_fields'] = ['body'];
      }

      // Remove email parameters to use the ones from template.
      if (!empty($message['params']['template_fields'])) {
        $this->applyTemplateFields($unione_message, $message['params']['template_fields']);
      }
    }

    // Make sure the files provided in the attachments array exist.
    if (!empty($message['params']['attachments'])) {
      $attachments = [];
      foreach ($message['params']['attachments'] as $attachment) {
        if (!empty($attachment['filepath']) && file_exists($attachment['filepath'])) {
          $file = $this->fileStorage->loadByProperties(['uri' => $attachment['filepath']]);
          $file = reset($file) ?: NULL;

          if ($file) {
            $attachments[] = [
              'type' => $file->getMimeType(),
              'name' => $file->getFilename(),
              'content' => base64_encode(file_get_contents($file->createFileUrl(FALSE))),
            ];
          }
        }
        elseif (!empty($attachment['filecontent']) && !empty($attachment['filename'])) {
          $attachments[] = [
            'type' => $attachment['filemime'],
            'name' => $attachment['filename'],
            'content' => base64_encode($attachment['filecontent']),
          ];
        }
      }

      if (count($attachments) > 0) {
        $unione_message['attachments'] = $attachments;
      }
    }

    if (!empty($this->config->get('use_inline_attachments')) && !empty($unione_message['body']['html'])) {
      $this->setInlineAttachments($unione_message);
    }

    // Set the header to track the users using this module.
    $message['headers']['X-Mailer'] = 'drupal-unione,devbranch-unione';

    if (!isset($message['headers']['to'])) {
      $message['headers']['to'] = $message['to'];
    }
    return [$unione_message, $message['headers']];
  }

  /**
   * Method to split email and sender name.
   *
   * @param string $email
   *   The email address to validate.
   *
   * @return array|null
   *   The email address if it is valid, or an array of error messages.
   *
   * @see \Symfony\Component\Mime\Address::create
   */
  public function checkEmail(string $email): ?array {
    if (!str_contains($email, '<')) {
      return [trim($email)];
    }

    if (!preg_match(self::FROM_STRING_PATTERN, $email, $matches)) {
      if ($this->isDebug) {
        $this->logger->error('Could not parse "%email" to a "%class" instance.',
          [
            '%email' => $email,
            '%class' => self::class,
          ]);
      }

      return NULL;
    }

    return [trim($matches['addrSpec']), trim($matches['displayName'])];
  }

  /**
   * Creates inline attachment for given file path.
   *
   * @param string $path
   *   Absolute path to the image.
   * @param string $image_id
   *   The attachment id.
   *
   * @return array|null
   *   Array with inline attachment info, or NULL if image was not found.
   */
  public function createInlineAttachment(string $path, string $image_id): ?array {
    if (UrlHelper::isValid($path)) {
      // Try loading file content from the provided path.
      @$content = file_get_contents($path);
      $mime_type = $this->mimeTypeGuesser->guessMimeType($path);

      if (!empty($mime_type) && !empty($content)) {
        return [
          'type' => $mime_type,
          'name' => $image_id,
          'content' => base64_encode($content),
        ];
      }
    }

    return NULL;
  }

  /**
   * Find and replace images with inline attachments.
   *
   * @param array $message
   *   Body string.
   */
  public function setInlineAttachments(array &$message): void {
    $html_dom = new \DOMDocument();
    $html_dom->loadHTML($message['body']['html']);
    // Extract all img elements / tags from the HTML.
    $image_tags = $html_dom->getElementsByTagName('img');

    if (count($image_tags) < 1) {
      return;
    }

    $id = 0;
    $inline_attachments = [];
    // Loop through the image tags that DOMDocument found.
    foreach ($image_tags as $image_tag) {
      // Generate attachment id, that will be used in "src" image attribute.
      $image_id = 'UNIONE-DRUPAL-IMAGECID' . $id++;
      // Get the src attribute of the image.
      $img_src = $image_tag->getAttribute('src');
      if ($attachment = $this->createInlineAttachment($img_src, $image_id)) {
        $inline_attachments[] = $attachment;
        $image_tag->setAttribute('src', "cid:$image_id");
      }
    }

    if (!empty($inline_attachments)) {
      $body_text = $html_dom->saveHTML();
      $message['body']['html'] = $body_text;
      $message['inline_attachments'] = array_merge($message['inline_attachments'],
        $inline_attachments);
    }
  }

  /**
   * Unsets email parameters to apply the template values.
   *
   * By default, email fields have higher priority than template parameters.
   * Therefore, if email has body, it'll be used instead of template body.
   *
   * This method removes some default email parameters to use template fields.
   * A list of email parameters can be controlled by
   * $message['params']['template_fields'] array item.
   *
   * @param array $message
   *   The Unione message array.
   * @param array $template_parameters
   *   A list of parameters to be removed from the email array.
   */
  public function applyTemplateFields(array &$message, array $template_parameters): void {
    $keys = ['body', 'from_email', 'from_name', 'subject'];

    foreach ($template_parameters as $item) {
      if (in_array($item, $keys) && !empty($message[$item])) {
        unset($message[$item]);
      }
    }
  }

}
