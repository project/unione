<?php

namespace Drupal\unione\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure UniOne settings for this site.
 */
class UniOneAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'unione.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unione_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unione.settings');

    $form['description'] = [
      '#markup' => $this->t('Please refer to link for your settings:
      if you use UniOne USA & Canada Instance - @us,
      or if you use UniOne European Instance - @eu', [
        '@us' => Link::fromTextAndUrl($this->t('dashboard'),
          Url::fromUri('https://us1.unione.io/en', [
            'attributes' => [
              'onclick' => "target='_blank'",
            ],
          ]))->toString(),
        '@eu' => Link::fromTextAndUrl($this->t('dashboard'),
          Url::fromUri('https://eu1.unione.io/en', [
            'attributes' => [
              'onclick' => "target='_blank'",
            ],
          ]))->toString(),
      ]),
    ];

    $form['api_key'] = [
      '#title' => $this->t('UniOne API Key'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
    ];

    $form['endpoint'] = [
      '#title' => $this->t('UniOne Instance'),
      '#type' => 'select',
      '#required' => TRUE,
      '#description' => $this->t('Select which UniOne Instance to use.'),
      '#options' => [
        'us1.unione.io' => $this->t('UniOne USA & Canada Instance'),
        'eu1.unione.io' => $this->t('UniOne European Instance'),
      ],
      '#default_value' => $config->get('endpoint'),
    ];

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['advanced_settings']['use_inline_attachments'] = [
      '#title' => $this->t('Use inline attachments'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('use_inline_attachments'),
      '#description' => $this->t('Enable to include images in email body instead of loading them from external URL.'),
    ];

    $form['advanced_settings']['tracking'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Tracking'),
    ];

    $form['advanced_settings']['tracking']['track_click'] = [
      '#title' => $this->t('Enable Track Clicks'),
      '#type' => 'select',
      '#options' => [
        '1' => $this->t('Yes'),
        '0' => $this->t('No'),
      ],
      '#default_value' => $config->get('track_click'),
      '#description' => $this->t('Enable to track the clicks of within an email.'),
    ];

    $form['advanced_settings']['tracking']['track_read'] = [
      '#title' => $this->t('Enable Track Reads'),
      '#type' => 'select',
      '#options' => [
        '1' => $this->t('Yes'),
        '0' => $this->t('No'),
      ],
      '#default_value' => $config->get('track_read'),
      '#description' => $this->t('Enable to track the reads of within an email.'),
    ];

    $form['advanced_settings']['debug_mode'] = [
      '#title' => $this->t('Enable Debug Mode'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('debug_mode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('unione.settings');
    $config_keys = [
      'api_key', 'endpoint', 'track_click', 'track_read', 'debug_mode', 'use_inline_attachments',
    ];

    foreach ($config_keys as $config_key) {
      if ($form_state->hasValue($config_key)) {
        $config->set($config_key, $form_state->getValue($config_key));
      }
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
