INTRODUCTION
------------
This module provides integration with the **[UniOne](https://unione.io/en/)** transactional email service.

INSTALLATION
------------
Use composer to download unione, which will download all the dependencies
  required: `composer require drupal/unione`

CONFIGURATION
-------------
1. You will need to put in your **[UniOne](https://unione.io/en/)** API key for your UniOne
   account.

   If you do not have a UniOne account, go to
   [https://unione.io](https://unione.io) and sign up for a
   new account. Once you have set up your account and are logged in, visit:
   'Account -> Security -> API Key' to generate a key.

2. Direct your browser to /admin/config/services/unione to configure the
   module.

3. Go to the Unione settings page (/admin/config/services/unione)
   and enter your API key into the corresponding field.

4. Select your UniOne instance (**it should match the server where account
was registered**).

5. Advanced settings:
  - Use inline attachments - allows to send inline attachments in body html. [More](https://docs.unione.io/en/web-api-ref#email-send)
  - Tracking settings - allows to enable clicks and reads tracking. [More](https://docs.unione.io/en/web-api-ref#email-send)
  - Enable Debug Mode - Includes recording logs about sending messages to
Drupal Reports.

6. Go to the Drupal Mail System config /admin/config/system/mailsystem and
in field **Sender** check UniOne mailer.

FOR DEVELOPERS
-------------

### Custom parameters

Unione plugin accepts some parameters, that will be processed and sent
with the message. You can find some examples below.

```php
/**
 * Implements hook_mail().
 */
function your_module_mail($key, &$message, $params) {
  // If "Use inline attachments" checkbox is enabled in module configuration,
  // the plugin will try to convert body images into inline attachments
  // automatically. However, it's also possible to prepare the list of
  // attachments manually.
  $message['params']['inline_attachments'] = [
    [
      "type" => "image/gif",
      "name" => "IMAGECID1",
      "content" => "R0lGODdhAwADAIABAP+rAP///ywAAAAAAwADAAACBIQRBwUAOw=="
    ]
  ];

  // This is how to apply Unione template to the email.
  // A list of available templates is available on Unione > Templates
  // admin page.
  $message['params']['template_id'] = 'Template id';
  // If template is set, Unione will use template values (subject, body, etc.)
  // only if they're not available in the email request data. If you want
  // to apply template fields, you can set the list in "template_fields"
  // parameter. If "template_fields" are not set, "body" value is applied.
  $message['params']['template_fields'] = ['body', 'from_email',
  'from_name', 'subject'];

  // By default, Unione plugin prepares a list of recipients based on default
  // "to" parameter. If you need to pass recipients with substitutions and
  // (or) metadata, you can use "recipients" params key.
  $message['params']['recipients'] = [
    [
      "email" => "user@example.com",
      "substitutions" => [
        "CustomerId" => 12452,
        "to_name" => "John Smith"
      ],
      "metadata" => [
        "campaign_id" => "c77f4f4e-3561-49f7-9f07-c35be01b4f43",
        "customer_hash" => "b253ac7"
      ]
    ],
  ];
}
```
