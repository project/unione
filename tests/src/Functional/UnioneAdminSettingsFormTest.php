<?php

namespace Drupal\Tests\unione\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Url;

/**
 * Tests that all provided admin pages are reachable.
 *
 * @group unione
 */
class UnioneAdminSettingsFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['unione'];

  /**
   * Permissions required by the user to perform the tests.
   *
   * @var array
   */
  protected $permissions = [
    'administer unione',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * An editable config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $unioneConfig;

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
    $this->unioneConfig = $this->config('unione.settings');
  }

  /**
   * Tests admin pages provided by Unione.
   */
  public function testSettingsFormSubmit() {
    $admin_user = $this->drupalCreateUser($this->permissions);
    $this->drupalLogin($admin_user);

    $this->drupalGet(Url::fromRoute('unione.admin_settings_form'));

    // Make sure that "API Key" field is visible and required.
    $api_key_field = $this->assertSession()->elementExists('css', 'input[name="api_key"]');
    $this->assertTrue($api_key_field->hasAttribute('required'), 'Field "UniOne API Key" must be required');

    // Make sure that "Endpoint" field is visible and required.
    $endpoint = $this->assertSession()->elementExists('css', 'select[name="endpoint"]');
    $this->assertTrue($endpoint->hasAttribute('required'), 'Field "UniOne Instance" must be required');

    // Save additional parameters. Check that all fields available on the form.
    $field_values = [
      'api_key' => 'dfispdoifdfksfsdlkfj;sdlpsdipos',
      'endpoint' => 'us1.unione.io',
      'track_click' => 1,
      'track_read' => 1,
      'debug_mode' => TRUE,
      'use_inline_attachments' => TRUE,
    ];

    $this->submitSettingsForm($field_values, 'The configuration options have been saved.');

    // Rebuild config values after form submit.
    $this->unioneConfig = $this->config('unione.settings');

    // Test that all field values are stored in configuration.
    foreach ($field_values as $field_name => $field_value) {
      $this->assertEquals($field_value, $this->unioneConfig->get($field_name));
    }
  }

  /**
   * Submits Unione settings form with given values and checks status message.
   */
  private function submitSettingsForm(array $values, $result_message) {
    foreach ($values as $field_name => $field_value) {
      $this->getSession()->getPage()->fillField($field_name, $field_value);
    }
    $this->getSession()->getPage()->pressButton('Save configuration');
    $this->assertSession()->pageTextContains($result_message);
  }

}
